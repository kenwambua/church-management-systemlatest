/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

package com.Api_Craft.dao.impl;

import com.Api_Craft.Generators.UuidGen;
import com.Api_Craft.dao.ChurchGroupsDAO;
import com.Api_Craft.models.ChurchGroups;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Benjamin on 2/7/2015.
 */
@Repository public class ChurchGroupsDAOImpl implements ChurchGroupsDAO {
	private static final Logger logger = Logger.getLogger(ChurchGroupsDAOImpl.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override public void addChurchGroups(ChurchGroups churchGroups) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String Uuid = new UuidGen().uuidGen();
			churchGroups.setChurchGroupUuid(Uuid);
			session.persist(churchGroups);
			logger.info("Churchgroup saved successfully, Churchgroup Details=" + churchGroups);
		} catch (RuntimeException ex) {
			throw ex;
		}
	}

	@Override public void updateChurchGroups(ChurchGroups churchGroups) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(churchGroups);
			logger.info("Churchgroup saved successfully, Churchgroup Details=" + churchGroups);
		} catch (RuntimeException ex) {
			session.getTransaction().rollback();
			throw ex;
		}
	}

	@SuppressWarnings("unchecked") @Override public List<ChurchGroups> listChurchGroups() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<ChurchGroups> churchGroupsList = session.createQuery("from churchgroups").list();
			for (ChurchGroups churchGroups : churchGroupsList) {
				logger.info("Person List::" + churchGroups);
			}
			return churchGroupsList;
		} catch (RuntimeException ex) {
			session.getTransaction().rollback();
			throw ex;
		}
	}

	@Override public ChurchGroups getChurchGroupByUuid(String churchGroupUuid) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			ChurchGroups churchGroups = (ChurchGroups)session.load(ChurchGroups.class, churchGroupUuid);
			logger.info("Churchgroup loaded successfully, Churchgroup Details=" + churchGroups);
			return churchGroups;
		} catch (RuntimeException ex) {
			session.getTransaction().rollback();
			throw ex;
		}
	}

	@Override public void removeChurchGroup(String churchGroupUuid) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			ChurchGroups churchGroups = (ChurchGroups)session.load(ChurchGroups.class, churchGroupUuid);
			if (null != churchGroups) {
				session.delete(churchGroups);
			}
			logger.info("Churchgroup deleted successfully, Churchgroup Details=" + churchGroups);
		} catch (RuntimeException ex) {
			session.getTransaction().rollback();
			throw ex;
		}
	}
}
